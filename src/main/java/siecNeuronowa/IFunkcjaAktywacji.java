package siecNeuronowa;

/**
 * Created by jarek on 31.12.13.
 */
public interface IFunkcjaAktywacji {
    double oblicz(double in);

    double oblicz(double[] in);

    double obliczPochodna(double out);
}
