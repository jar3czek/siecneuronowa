package siecNeuronowa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Hello world!
 */
public class Main {
    public static void main(String[] args) {
        SiecNeuronowa siecNeuronowa = new SiecNeuronowa(5, 5, 1, new FunkcjaSigmoidalnaUnipolarna());
        siecNeuronowa.losujWagi(0, 1);

        File file = new File(args[0]);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<String> input = new ArrayList<String>();
        List<double[]> dane = new ArrayList<>();
        double[] oczekiwaneWyjscie = new double[1];

        int iterator = 0;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            double[] temp = new double[7];
            if (line.length() > 1) {
                for (int i = 0; i < 7; i++) {
                    temp[i] = Double.parseDouble(line.substring(i, i + 1));
                    //System.out.println("added " + temp[i]);
                }
                dane.add(temp);
            } else {
                if (line.length() == 1) {
                    oczekiwaneWyjscie[0] = Double.parseDouble(line);
                    iterator++;
                    System.out.println("Zestaw danych nr : " + iterator);
                    System.out.println("oczekiwane wyjscie: " + oczekiwaneWyjscie[0]);
                    siecNeuronowa.uczSie(oczekiwaneWyjscie, dane);
                    dane = new ArrayList<>();
                }

            }
        }


    }
}
