package siecNeuronowa;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jarek on 31.12.13.
 */
public class Neuron {
    public ArrayList<Polaczenie> in;
    public double out;
    public IFunkcjaAktywacji funkcjaAktywacji;
    public double blad;
    public static double wspolczynnikNauki = 0.1;
    public double wagaBiasu;

    public Neuron(IFunkcjaAktywacji funkcjaAktywacji) {
        this.funkcjaAktywacji = funkcjaAktywacji;
        in = new ArrayList();
        out = 0.0;
        blad = 0.0;
        wagaBiasu = 0.0;
    }

    public void dodajWejscie(Neuron neuron, double waga) {
        in.add(new Polaczenie(neuron, waga));
    }

    public void dodajWejscie(Neuron neuron) {
        dodajWejscie(neuron, 1.0);
    }

    public void losujWagi(double min, double max) {
        Random random = new Random();
        for (Polaczenie polaczenie : in) {
            polaczenie.waga = (random.nextDouble() * (max * 100 - min * 100) + min * 100) / 100;
            System.out.println("Wylosowano wage: " + polaczenie.waga);
        }
    }

    public void obliczWyjscie() {
        out = 0.0;
        for (Polaczenie polaczenie : in) {
            out += polaczenie.neuron.out * polaczenie.waga;
        }
        out += wagaBiasu;
        out = funkcjaAktywacji.oblicz(out);
    }

    public void obliczBlad(double oczekiwaneWyjscie) {
        blad = oczekiwaneWyjscie - out;
    }

    public void poprawWagi() {
        for (Polaczenie polaczenie : in) {
            polaczenie.waga += wspolczynnikNauki * blad * funkcjaAktywacji.obliczPochodna(out) * polaczenie.neuron.out;
        }
        wagaBiasu += wspolczynnikNauki * blad * funkcjaAktywacji.obliczPochodna(out);
    }

    public void dodajWejscie(Warstwa warstwa) {
        for (Neuron neuron : warstwa.neurony) dodajWejscie(neuron);
    }

    public void wstecznaPropagacja() {
        for (Polaczenie polaczenie : in) {
            polaczenie.neuron.blad += blad * polaczenie.waga;
        }
    }

}
