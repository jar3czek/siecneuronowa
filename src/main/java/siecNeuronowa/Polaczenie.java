package siecNeuronowa;

/**
 * Created by jarek on 02.01.14.
 */
public class Polaczenie {
    public Neuron neuron;
    public double waga;

    public Polaczenie(Neuron neuron, double waga) {
        this.neuron = neuron;
        this.waga = waga;
    }
}
