package siecNeuronowa;

import java.util.List;

/**
 * Created by jarek on 02.01.14.
 */
public class SiecNeuronowa {
    public Warstwa warstwaWejsciowa, warstwaUkryta, warstwaWyjsciowa;

    public SiecNeuronowa(int iloscWejsc, int iloscUkrytch, int iloscWyjsc, IFunkcjaAktywacji funkcjaAktywacji) {
        warstwaWejsciowa = new Warstwa(iloscWejsc, funkcjaAktywacji);
        warstwaUkryta = new Warstwa(iloscUkrytch, funkcjaAktywacji);
        warstwaWyjsciowa = new Warstwa(iloscWyjsc, funkcjaAktywacji);

        warstwaWyjsciowa.polaczZWarstwaNizej(warstwaUkryta);
        warstwaUkryta.polaczZWarstwaNizej(warstwaWejsciowa);
    }

    public void losujWagi(double min, double max) {
        warstwaUkryta.losujWagi(min, max);
        warstwaWyjsciowa.losujWagi(min, max);
    }

    public double[] obliczWyjscia(List<double[]> in) {
        warstwaWejsciowa.ustawWarstweWejsciowa(in);
        warstwaUkryta.obliczWyjscia();
        warstwaWyjsciowa.obliczWyjscia();

        return warstwaWyjsciowa.zwrocWyjscia();
    }

    public void uczSie(double[] poprawneWyjscie, List<double[]> dane) {
        warstwaWyjsciowa.zerujBledy();
        warstwaUkryta.zerujBledy();


        //warstwaWejsciowa.ustawWarstweWejsciowa(dane);
        obliczWyjscia(dane);

        warstwaWyjsciowa.obliczBledy(poprawneWyjscie);
        System.out.println("Blad warstwy wyjsciowej: " + warstwaWyjsciowa.neurony.get(0).blad);
        warstwaWyjsciowa.wstecznaPropagacja();
        warstwaUkryta.wstecznaPropagacja();

        warstwaWyjsciowa.poprawWagi();
        warstwaUkryta.poprawWagi();

    }
}
