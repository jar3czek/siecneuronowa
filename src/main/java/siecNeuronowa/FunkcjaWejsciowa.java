package siecNeuronowa;

/**
 * Created by jarek on 05.01.14.
 */
public class FunkcjaWejsciowa implements IFunkcjaAktywacji {
    @Override
    public double oblicz(double in) {
        return 0;
    }

    @Override
    public double oblicz(double[] in) {
        double wartosc = 0.0;
        int mul = 1;
        for (int i = in.length - 1; i >= 0; i--) {
            wartosc += in[i] * mul;
            mul *= 2;
        }

        //skalowanie wartosci do przedzialu <0,1>
        wartosc = wartosc * 0.01 - 0.4;


        return wartosc;
    }

    @Override
    public double obliczPochodna(double out) {
        return 0;
    }
}
