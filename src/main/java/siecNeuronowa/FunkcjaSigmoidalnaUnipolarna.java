package siecNeuronowa;

/**
 * Created by jarek on 31.12.13.
 */
public class FunkcjaSigmoidalnaUnipolarna implements IFunkcjaAktywacji {
    public double beta;

    public FunkcjaSigmoidalnaUnipolarna() {
        beta = 1.0;
    }

    public FunkcjaSigmoidalnaUnipolarna(double beta) {
        this.beta = beta;
    }

    @Override
    public double oblicz(double in) {
        return 1 / (1 + Math.exp(-beta * in));
    }

    @Override
    public double oblicz(double[] in) {
        return 0;
    }

    @Override
    public double obliczPochodna(double out) {
        return out * (1.0 - out);
    }
}
