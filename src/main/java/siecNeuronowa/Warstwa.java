package siecNeuronowa;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jarek on 02.01.14.
 */
public class Warstwa {
    public ArrayList<Neuron> neurony;

    public Warstwa(int iloscNeuronow, IFunkcjaAktywacji funkcjaAktywacji) {
        neurony = new ArrayList<Neuron>();
        for (int i = 0; i < iloscNeuronow; i++) {
            dodajNeuron(new Neuron(funkcjaAktywacji));
        }
    }

    public void dodajNeuron(Neuron neuron) {
        neurony.add(neuron);
    }

    public void polaczZWarstwaNizej(Warstwa warstwaNizej) {
        for (Neuron neuron : neurony) {
            neuron.dodajWejscie(warstwaNizej);
        }
    }

    public void losujWagi(double min, double max) {
        for (Neuron neuron : neurony) {
            neuron.losujWagi(min, max);
        }
    }

    public void ustawWarstweWejsciowa(List<double[]> in) {
        IFunkcjaAktywacji funkcjaWejsciowa = new FunkcjaWejsciowa();
        for (int i = 0; i < 5; i++) {
            neurony.get(i).out = funkcjaWejsciowa.oblicz(in.get(i));
            System.out.println("Neuron nr " + (i + 1) + " wyplul wartosc: " + neurony.get(i).out);
        }
    }

    public double[] zwrocWyjscia() {
        double[] out = new double[neurony.size()];
        for (int i = 0; i < neurony.size(); i++) {
            out[i] = neurony.get(i).out;
        }
        return out;
    }

    public void obliczWyjscia() {
        for (Neuron neuron : neurony) {
            neuron.obliczWyjscie();
            System.out.println("Neuron wyplul wartosc: " + neuron.out);
        }
    }

    public void zerujBledy() {
        for (Neuron neuron : neurony) {
            neuron.blad = 0.0;
        }
    }

    public void obliczBledy(double[] oczekiwaneWyjscie) {
        for (int i = 0; i < neurony.size(); i++) {
            neurony.get(i).obliczBlad(oczekiwaneWyjscie[i]);
        }
    }

    public void poprawWagi() {
        for (Neuron neuron : neurony) {
            neuron.poprawWagi();
        }
    }

    public void wstecznaPropagacja() {
        for (Neuron neuron : neurony) {
            neuron.wstecznaPropagacja();
        }
    }

}
